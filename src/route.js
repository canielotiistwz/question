import Home from './components/Home.vue'
import AddProduct from './components/addProduct.vue'
import EditeProduct from './components/products/editeProducts.vue'
import Cart from './components/cart.vue'
export const routes = [
    { path: '/', component: Home, },
    { path: '/cart', component: Cart },
    { path: '/product', component: AddProduct, },
    { path: "/product/:id/edite", component: EditeProduct }

]

