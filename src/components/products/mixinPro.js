export const inputSearch = {
    data() {
        return {
            searchIn: "",
            products: [
                {
                    title: "Panasonic Lumix DC-FZ80 Digital Camera Lumix DC Vario 60x Zoom Lens",
                    price: 297.99,
                    count: 5,
                    cart: 'https://andrimann.ir/wp-content/uploads/2020/08/onlineshopping-1.png',
                    pic: "https://www.bhphotovideo.com/images/images2500x2500/panasonic_dc_fz80k_lumix_dmc_fz80_digital_camera_1304874.jpg",
                },
                {
                    title: "Olympus OM-D E-M1 Mark III Mirrorless Digital Camera ",
                    price: 1.699,
                    count: 40,
                    cart: 'https://andrimann.ir/wp-content/uploads/2020/08/onlineshopping-1.png',
                    pic: 'https://www.bhphotovideo.com/images/images2500x2500/senal_sc_550x_lg_dia_condenser_mic_1058355.jpg'
                },
                {
                    title: "Senal SC-550X Professional Cardioid Condenser Microphone ",
                    price: 99.99,
                    count: 25,
                    cart: 'https://andrimann.ir/wp-content/uploads/2020/08/onlineshopping-1.png',
                    pic: "https://www.bhphotovideo.com/images/images2500x2500/senal_sc_550x_lg_dia_condenser_mic_1058355.jpg",
                },
                {
                    title: "ASUS Designo MZ27AQL 27' 16:9 IPS Monitor",
                    price: 299.95,
                    count: 14,
                    cart: 'https://andrimann.ir/wp-content/uploads/2020/08/onlineshopping-1.png',
                    pic: "https://www.bhphotovideo.com/images/images2500x2500/asus_designo_mz27aql_27_wqhd_1429422.jpg",
                },
                {
                    title: "LG UP8770PU 86' Class HDR 4K UHD Smart LED TV",
                    price: 1.69699,
                    count: 10,
                    cart: 'https://andrimann.ir/wp-content/uploads/2020/08/onlineshopping-1.png',
                    pic: "https://www.bhphotovideo.com/images/images2500x2500/lg_86up8770pua_up8770pu_86_class_hdr_1632392.jpg",
                },
                {
                    title: "AKG P220 Large-Diaphragm Cardioid Condenser Microphone",
                    price: 135.0,
                    count: 4,
                    cart: 'https://andrimann.ir/wp-content/uploads/2020/08/onlineshopping-1.png',
                    pic: "https://www.bhphotovideo.com/images/images2500x2500/akg_3101h00420_p220_cardioid_condenser_microphone_1083344.jpg",
                },
            ],
        };
    },
    computed: {
        filterdProducts() {
            return this.products.filter((item) => {
                return item.title.toLowerCase().match(this.searchIn);
            });
        },
    },
}